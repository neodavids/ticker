<div id="sidebar" class="active">
    <div class="sidebar-wrapper active">
<div class="sidebar-header">
<div class="d-flex justify-content-between">
    <div class="logo">
        <a href="index.html"><img src="{{ asset('assets/images/logo/null.png') }}" srcset="">NC-TICKER</a>
    </div>
    <div class="toggler">
        <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
    </div>
</div>
</div>
<div class="sidebar-menu">
<ul class="menu">
    <li class="sidebar-title">Menu</li>

    <li
        class="sidebar-item active ">
        <a href="index.html" class='sidebar-link'>
            <i class="bi bi-grid-fill"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li
        class="sidebar-item  has-sub">
        <a href="#" class='sidebar-link'>
            <i class="bi bi-stack"></i>
            <span>Posts</span>
        </a>
        <ul class="submenu ">
            <li class="submenu-item ">
                <a href="{{route('posts.create') }}"> Add a New  Post</a>
            </li>
            <li class="submenu-item ">
                <a href="{{route('posts.index') }}">All Posts</a>
            </li>
            <li class="submenu-item ">
                <a href="{{route('posts.index', ['filter'=>'pending']) }}">Pending Posts</a>
            </li>
            <li class="submenu-item ">
                <a href="{{route('posts.index', ['filter'=>'published']) }}">Published Posts</a>
            </li>

        </ul>
    </li>

    <li
        class="sidebar-item  has-sub">
        <a href="#" class='sidebar-link'>
            <i class="bi bi-collection-fill"></i>
            <span>Categories</span>
        </a>
        <ul class="submenu ">
            @foreach ( App\Models\Category::all() as $category )


            <li class="submenu-item ">
                <a href="#">{{$category->name}}</a>
            </li>
            @endforeach
        </ul>
    </li>

    <li
        class="sidebar-item  has-sub">
        <a href="#" class='sidebar-link'>
            <i class="bi bi-grid-1x2-fill"></i>
            <span>Users</span>
        </a>
        <ul class="submenu ">
            @foreach ( App\Models\User::all() as $user )


            <li class="submenu-item ">
                <a href="#">{{$user->name}}</a>
            </li>
            @endforeach
        </ul>
    </li>

    <li
        class="sidebar-item ">
        <form method="POST" action="{{ route('logout') }}">
            @method('POST')
            @csrf
            <a type="submit" class='sidebar-link'>
                <i class="bi bi-power"></i>
                <span>Logout</span>
            </a>
    </form>
    </li>
</ul>
</div>
<button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
</div>
</div>
