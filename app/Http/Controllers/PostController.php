<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        $pending_posts = Post::all()->where('status','=','pending');
        $published_posts = Post::all()->where('status','=','published');
        return view('dashboard.posts.index',compact('posts','published_posts','pending_posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('dashboard.posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'body'           => 'required',
            'category'           => 'required',
        ]);

        $user = auth()->user();
        $post = new Post();
        $post->body = $request->input('body');
        $post->category_id = $request->input('category');
        $post->status = 'pending';
        $post->user_id = $user->id;
        $post->save();
        $request->session()->flash('message', 'Ticker Post Added!');
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $post = Post::find($id);
        return view('dashboard.posts.edit',compact('categories','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'body'           => 'required',
            'category'           => 'required',
        ]);

        $post = Post::find($id);
        $post->body = $request->input('body');
        $post->category_id = $request->input('category');
        $post->status = $request->input('status');
        $post->save();
        $request->session()->flash('message', 'Ticker Post Status Updated!');
        return redirect()->route('posts.index');
    }

    public function approval($id)
    {
        $categories = Category::all();
        $post = Post::find($id);
        return view('dashboard.posts.approve',compact('categories','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        $validatedData = $request->validate([
            'body'           => 'required',
            'category'           => 'required',
            'status'            => 'required'
        ]);

        $post = Post::find($id);
        $post->body = $request->input('body');
        $post->category_id = $request->input('category');
        $post->status = $request->input('status');
        $post->save();
        $request->session()->flash('message', 'Ticker Post  Updated!');
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
